import scala.collection.mutable.ListBuffer
import scala.io.Source._
import java.io._

object scalalab extends App {

  val writer = new PrintWriter(new File("output.tsv"))
  val rr = for {
    tr <- prepareTr()
    r <- prepareRange()
    if cmpIp(r._1, r._2, tr._2)
  } yield writer.write(s"${tr._1}\t${r._3}\n")

  writer.close()

  def prepareRange(): List[(String, String, String)] = {
    val rangesFile = fromFile("ranges.tsv").getLines().toList
    val sp = rangesFile.map(_.split("\t")).map { x =>
      val firstIp = x(0).split("-")(0)
      val secondIp = x(0).split("-")(1)
      val segmentName: String = x(1)
      (firstIp, secondIp, segmentName)
    }
    sp
  }

  def prepareTr(): List[(Long, String)] = {
    val transactionFile = fromFile("transactions.tsv").getLines().toList
    val splited = transactionFile.map(_.split("\t")).map(x => (x(0).toLong, x(1)))
    splited
  }

  def cmpIp(start: String, stop: String, check: String): Boolean = {
    (ip2L(check) >= ip2L(start)) && (ip2L(check) <= ip2L(stop))
  }

  def ip2L(ip: String): Long = {
    val oct: Array[Long] = ip.split("\\.").map(java.lang.Long.parseLong(_))
    val ret: Long = (3 to 0 by -1).foldLeft(0L)((a, b) => a | (oct(3 - b) << b * 8))

    ret & 0xFFFFFFFF
  }

  def l2Ip(ip: Long): String = {
    val resultBuilder = new ListBuffer[String]()
    var ipBuffer = ip

    for (position <- 0 to 3) {
      resultBuilder.prepend((ipBuffer & 0xFF).toString)
      ipBuffer >>= 8
    }

    resultBuilder.mkString(".")
  }
}
