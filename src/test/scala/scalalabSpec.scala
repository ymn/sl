import org.scalatest._
import scalalab._

object Helper {
  def generateIp(start: Int, stop: Int): IndexedSeq[String] = {
    for {
      a <- start to stop
      b <- start to stop
      c <- start to stop
      d <- start to stop
    } yield List(a, b, c, d).mkString(".")
  }
}

class scalalabSpec extends FlatSpec with Matchers {
  "ip2L" should "convert dot-separated ip-address to decimal representation" in {
    ip2L("192.168.1.16") == 3232235792L
    ip2L("10.10.10.10") == 168430090L
  }

  "l2Ip" should "convert decimal representation to dot-separated" in {
    l2Ip(3232235792L) == "192.168.1.16"
    l2Ip(168430090L) == "10.10.10.10"
  }

  "decimal representation" should "be ordered" in {
    ip2L("192.168.1.10") > ip2L("192.168.1.9")
    ip2L("192.168.1.1") < ip2L("192.168.255.255")
  }

  import Helper._
  "result of ip2L(l2Ip(x)" should "be equal x" in {
    generateIp(0, 5).foreach{ x =>
      l2Ip(ip2L(x)) == x
    }

  }

}